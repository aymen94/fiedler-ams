import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { dataTableService } from './../services/dataTable.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  providers:[dataTableService],
  styleUrls: ['./table.component.css']
})
export class TableComponent{
  public dataView;
  constructor(private dataT:dataTableService) {
    this.getData();
   }

  public getData(){
    this.dataT.loadData().subscribe(arg => this.dataView = arg);    
  }
}
