var express = require('express');
var router = express.Router();
var data = require('../model/data');

router.get('/:id?',  (req, res, next)=> {
	if (req.params.id)
		data.getInfoById(req.params.id,(err, rows)=>res.json(rows));
	else
		data.getAllData((err, rows)=>res.json(rows));
	
});

module.exports = router;