-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 12, 2018 alle 14:56
-- Versione del server: 10.1.30-MariaDB
-- Versione PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fiedler_ams`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `psh_raw`
--

CREATE TABLE `psh_raw` (
  `rid` int(11) NOT NULL COMMENT 'Row unique id',
  `fk_user` int(11) NOT NULL COMMENT 'Reference to user table',
  `time` datetime NOT NULL,
  `direct` int(11) NOT NULL COMMENT 'Enter = 1; Leave = 0',
  `type` int(11) NOT NULL COMMENT 'Type of leave: General = 1;Vacation = 2;\r\nDoctor = 3...'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `psh_raw`
--

INSERT INTO `psh_raw` (`rid`, `fk_user`, `time`, `direct`, `type`) VALUES
(123, 11, '2018-02-08 18:24:25', 1, 1),
(1, 23, '2018-02-08 10:15:25', 1, 3),
(2, 324, '2018-02-06 03:16:22', 0, 3),
(3, 567, '2018-02-01 14:10:59', 0, 1),
(4, 76, '2018-02-22 13:00:00', 0, 2),
(5, 66, '2018-02-08 06:23:16', 1, 1),
(6, 677, '2018-02-08 16:00:00', 1, 2);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `psh_raw`
--
ALTER TABLE `psh_raw`
  ADD PRIMARY KEY (`rid`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `psh_raw`
--
ALTER TABLE `psh_raw`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Row unique id', AUTO_INCREMENT=124;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
