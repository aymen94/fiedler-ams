import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class dataTableService {

    constructor(private http:Http) { }


    public loadData(id=""):Observable<any>{
        return this.http.get('/api/'+id).map((res:Response)=>res.json());
    }
}