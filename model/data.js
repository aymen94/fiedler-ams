var db = require('./dbconnection');

var Data = {
	 getAllData: function (callback) {
		return db.query("Select * from psh_raw", callback);
	},
	getInfoById: function (id, callback) {
		return db.query("select * from psh_raw where rid=?", [id], callback);
	},
};

module.exports=Data;